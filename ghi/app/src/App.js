import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import ShoeForm from './ShoeForm';
import Nav from './Nav';
import ShoeList from "./ShoeList";
import HatsList from "./HatLists";
import HatsForm from "./HatForms";
import "./App.css";


function App(props) {
    if (props.shoes === undefined) {
      return null;
    }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/shoes" element={<ShoeList shoes={props.shoes} />} />
          <Route path="/hats" element={<HatsList hats={props.hats} />} />
          <Route path="/hats/new" element={<HatsForm />} />
          <Route path="/shoes/new" element={<ShoeForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;