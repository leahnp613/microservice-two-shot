from django.urls import path

from shoes_rest.views import (
    api_list_shoes,
    api_shoes_details,
)

urlpatterns = [
    path("shoes/", api_list_shoes, name="api_list_shoes"),
    path("shoes/<int:pk>/", api_shoes_details, name="api_shoes_details"),
]
