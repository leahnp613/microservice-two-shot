import React, {useEffect, useState } from 'react';

function ShoeForm () {

  const [manufacturer, setmanufacturer] = useState('');
  const [model, setmodel] = useState('');
  const [bins, setbins] = useState([]);
  const [color, setcolor] = useState('')
  const [picture_url, setpicture_url] = useState('');
  const [selectedBin, setselectedBin] = useState('');

    const data = {};
    data.manufacturer = manufacturer;
    data.model = model;
    data.color = color;
    data.picture_url = picture_url;
    data.bin = selectedBin;

    const url = 'http://localhost:8080/api/shoes/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const handleSubmit = async (event) => {
        event.preventDefault();

    const response = await fetch(url, fetchConfig);
    if (response.ok) {

      setmodel('');
      setmanufacturer('');
      setcolor('');
      setpicture_url('');
      setselectedBin('');
      setbins([]);

    }
};
    const fetchData = async () => {
        const url = 'http://localhost:8080/api/bins/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setbins(data.bins);
        }
      }

      useEffect(() => {
        fetchData();
      }, []);

  // How can we refactor these handleChange methods to make
  // a single method, like the ConferenceForm above?
  const handlemodelChange = (event) => {
    const value = event.target.value;
    setmodel(value);
  }

  const handlemanufacturerChange = (event) => {
    const value = event.target.value;
    setmanufacturer(value);
  }

  const handlecolorChange = (event) => {
    const value = event.target.value;
    setcolor(value);
  }

  const handlepicture_urlChange = (event) => {
    const value = event.target.value;
    setpicture_url(value);
  }




  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new Shoe</h1>
          <form onSubmit={handleSubmit} id="create-shoe-form">
            <div className="form-floating mb-3">
              <input value={model} onChange={handlemodelChange} placeholder="model" required type="text" name="model" id="model" className="form-control" />
              <label htmlFor="model">model</label>
            </div>
            <div className="form-floating mb-3">
              <input value={manufacturer} onChange={handlemanufacturerChange} placeholder="manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
              <label htmlFor="manufacturer">manufacturer</label>
            </div>
            <div className="form-floating mb-3">
              <input value={color} onChange={handlecolorChange} placeholder="color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="color">color</label>
            </div>
            <div className="form-floating mb-3">
              <input value={picture_url} onChange={handlepicture_urlChange} placeholder="picture_url" required type="text" name="picture_url" id="picture_url" className="form-control" />
              <label htmlFor="picture_url">picture_url</label>
            </div>
            <div className="mb-3">
                <select onChange={(e) => setselectedBin(e.target.value)} required id="bins" name="bins" className="form-select" value={selectedBin}>
                <option value="">Choose a bin</option>
                {bins.map((bin) => {
                  return (
                    <option key={bin.number} value={bin.number}>
                      {bin.closet_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default ShoeForm;
