import React, { useEffect, useState } from 'react';

function HatsForm(props) {
  const [states, setStates] = useState([]);
  const [fabric, setFabric] = useState('');
  const [styleName, setStyleName] = useState('');
  const [color, setColor ] = useState('');
  const [pictureUrl, setPictureUrl ] = useState('');
  const [location, setLocation] = useState('');
  const handleSubmit = async (event) => {
    event.preventDefault();
    
    const data = {};
    data.fabric = fabric;
    data.styleName = styleName;
    data.color = color;
    data.pictureUrl = pictureUrl
    data.location = location;
    console.log(data);

     const locationUrl = 'http://localhost:8090/api/hats';
     const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
     };

     const response = await fetch (locationUrl, fetchConfig);
     if (response.ok) {
        const newLocation = await response.json();
        console.log(newLocation);

        setFabric('');
        setStyleName('');
        setColor('');
        setPictureUrl('');
        setLocation('');
     }
  }

  const fetchData = async () => {
    const url = 'http://localhost:8090/api/locations/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setStates(data.states);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleFabricChange = (event) => {
    const value = event.target.value;
    setFabric(value);
  };
  const handleStyleNameChange = (event) => {
    const value = event.target.value;
    setStyleName(value);
  };
  const handleColorChange =(event) => {
    const value = event.target.value;
    setColor(value);
  }
  const handlePictureUrlChange =(event) => {
    const value = event.target.value;
    setPictureUrl(value);
  }
  const handleLocationChange =(event) => {
    const value = event.target.value;
    setLocation(value);
  }
console.log(states)
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new location</h1>
          <form onSubmit={handleSubmit} id="create-location-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleFabricChange}
                value = {fabric}
                placeholder="Fabric"
                required
                type="text"
                name="fabric"
                id="fabric"
                className="form-control"
              />
              <label htmlFor="fabric">Fabric</label>
            </div>
            <div className="form-floating mb-3">
              <input 
                onChange ={handleStyleNameChange}
                value ={styleName}
                placeholder="Style Name"
                required
                type="name"
                name="style_name"
                id="style_name"
                className="form-control"
              />
              <label htmlFor="style_name">Style Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange = {handleColorChange}
                value= {color}
                placeholder="Color"
                required
                type="text"
                name="color"
                id="color"
                className="form-control"
                />
            <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange = {handlePictureUrlChange}
                value= {pictureUrl}
                placeholder="Picture Url"
                required
                type="text"
                name="picture_url"
                id="picture_url"
                className="form-control"
        />
            <label htmlFor="picture_url">Picture Url</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange = {handleLocationChange}
                value= {location}
                placeholder="Location"
                required
                type="text"
                name="location"
                id="location"
                className="form-control"
        />
            <label htmlFor="location">Location</label>
            </div>
            <button className="btn btn-primary">Create</button>
            </form>
        </div>
        </div>
    </div>
)}

export default HatsForm;