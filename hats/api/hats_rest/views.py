from django.shortcuts import render
from .models import LocationVO, Hats
from common.json import ModelEncoder
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

class LocationEncoderVO(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name"
    ]

class HatsEncoder(ModelEncoder):
    model = Hats
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
        "id",
    ]
    encoders= {
        "location":LocationEncoderVO(),
    }

@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder = HatsEncoder,
        )
    else:
        content = json.loads(request.body)
        print(content)
        try:
            location_id = content["location"]
            location_href = f"/api/locations/{location_id}/"
            location = LocationVO.objects.get(import_href=location_href)
            print(location)
            content["location"] = location

        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        hat = Hats.objects.create(**content)
        return JsonResponse(
                hat,
                encoder=HatsEncoder,
                safe=False,
            )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_hats(request,pk):
    if request.method == "GET":
        hats = Hats.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder= HatsEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hats.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "location" in content:
                location = LocationVO.objects.get()
                content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location"},
                status=400,
            )
        Hats.objects.filter(id=pk).update(**content)
        hat = Hats.objects.get(id=pk)
        return JsonResponse(
            hats,
            encoder= HatsEncoder,
            safe=False,
        )
